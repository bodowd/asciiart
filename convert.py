from PIL import Image
import argparse
import numpy as np

# define grayscale levels and grid - 70 levels of gray
gscale1 = "$@B%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/\|()1{}[]?-_+~<>i!lI;:,\"^`'. "
gscale2 = "@%#*+=-:. "

def openImageDims(fileName, cols, scale):
    image = Image.open(fileName).convert("L") # L is luminance
    # image dimensions
    W, H = image.size[0], image.size[1]
    # tile width
    width = W/cols
    # tile height
    height = width/scale
    # number of rows in final grid
    rows = int(H/height)
    return image, W, H, width, height, rows

def getAverageL(image):
    im = np.array(image)
    w,h = im.shape
    return np.average(im.reshape(w*h))

def generate(width,height,W, H, rows,cols,image, lessLevels):
    aimg = []

    for j in range(rows):
        y1 = int(j*height)
        y2 = int((j+1)*height)
        # correct the last tile
        if j == rows-1:
           y2 = H
        aimg.append("")
        for i in range(cols):
            # crop img to fit the tile
            x1 = int(i*width)
            x2 = int((i+1)*width)
            # correct the last tile
            if i == cols-1:
                x2 = W
            # crop img to extract tile reached at the current iteration into another Image object
            img = image.crop((x1, y1, x2, y2))
            # get avg luminance
            avg = int(getAverageL(img))
            # scale down avg brightness and look up ASCII character
            if lessLevels:
                gval = gscale1[int((avg * 9) / 255)]
            else:
                gval = gscale1[int((avg*69)/255)]
            aimg[j] += gval
    return aimg

def writeToFile(outFile, aimg):
    with open(outFile, 'w') as f:
        for row in aimg:
            f.write(row + '\n')

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--file', dest = 'imgFile', required=True)
    parser.add_argument('--scale', dest = 'scale', required=False)
    parser.add_argument('--out', dest = 'outFile', required=False)
    parser.add_argument('--lessLevels', dest = 'lessLevels', action='store_true')
    parser.add_argument('--cols', dest = 'cols', required=False) # number of text columns in ASCII output
    args = parser.parse_args()

    imgFile = args.imgFile
    outFile = imgFile.split('.')[0] + '_ascii.txt'
    if args.outFile:
        outFile = str(args.outFile)
    scale = 1
    if args.scale:
        scale = float(args.scale)
    cols = 80
    if args.cols:
        cols = int(args.cols)
    if args.lessLevels:
        lessLevels = True
    else:
        lessLevels = False
    print('Generating...')
    image, W, H, width, height, rows = openImageDims(imgFile, cols, scale)
    aimg = generate(width, height, W, H, rows, cols, image, lessLevels)
    writeToFile(outFile, aimg)


if __name__ == "__main__":
    main()